'''
Script to accept an IPv4 address, determine what network it belongs to,
and display both the IP address and its network back to the user.

#This is the preliminary step to creating a more involved script#
'''
import ipaddress    #import the ipaddress module
import sys

#main function definition
def main(): 

    user_address = sys.argv[1]
    ip_add = ipaddress.ip_interface(user_address)

    network_address = get_network(ip_add) #call get_network function to get network address

    display_results(ip_add, network_address)               #call dispaly function to display info to user

#function definitions
 
def get_network(user_address):                                                      #function to determine the network address based on user entered IP address
    net_address = user_address.network                                              #determine the network address using the .network method on the user supplied IP address
    return net_address                                                              #return value to main()

def display_results(ip_add, network_address):                                 #function to display information back to user
    print('The IPv4 address you entered was {}'.format(ip_add))
    print('The network the address belongs to is {}'.format(network_address))
    
    
main()



