#################################################
READ ME
#################################################

This is a script to accept an IPv4 address and 
determine the network it belongs to. This was step
one to creating a script that would then take the 
network address and compare it to the ip addresses
listed in a given json file. The aim was to be able 
to verify whether a given ip address belonged to 
an Amazon network.

I have recently managed to get the script to search 
the given json file for the network address calculated
from the ip address entered. I have also broken the 
script into several versions. Mainly just for the heck
of it but also because I find the simple version handy 
to use.  

Mike Furtado

