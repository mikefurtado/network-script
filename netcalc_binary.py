'''
Script to accept an IPv4 address, determine what network it belongs to,
and display both the IP address and its network back to the user.

#This is the preliminary step to creating a more involved script#
'''
import ipaddress    #import the ipaddress module

#main function definition
def main():
    print('#' * 65)                                                            #display a header
    print('Enter IPv4 address in the following format: X.X.X.X/CIDR')
    print('#' * 65)

    user_address = get_address()    #call get_address function to get IP address from user
    user_add_bin = get_user_bin(user_address)   #call function to convert user address to binary
    
    network_address = get_network(user_address) #call get_network function to get network address
    net_add_bin = get_net_bin(network_address)  #call function to convert network address to binary
    
    display_results(user_address, network_address, user_add_bin, net_add_bin)   #call dispaly function to display info to user

#function definitions
def get_address():
    address = input('Enter an IPv4 address and CIDR value in the above format: ')   #function to prompt user for an IP address
    ip_address = ipaddress.ip_interface(address)                                    #comment on this line
    return ip_address                                                               #return value to main()

def get_user_bin(user_add):
    user_bin = str(user_add)
    no_cidr = user_bin[:-3]
    bin_add = bin(int(ipaddress.IPv4Address(no_cidr)))
    return bin_add[2:]
 
def get_network(user_address):                  #function to determine the network address based on user entered IP address
    net_address = user_address.network          #determine the network address using the .network method on the user supplied IP address
    return net_address                          #return value to main()

def get_net_bin(net_add):
    net_bin = str(net_add)
    no_cidr = net_bin[:-3]
    bin_net = bin(int(ipaddress.IPv4Address(no_cidr)))
    return bin_net[2:]

def display_results(user_address, network_address, u_bin, n_bin):           #function to display information back to user
    print('The IPv4 address you entered was {}'.format(user_address))
    print('The network the address belongs to is {}'.format(network_address))
    print('')
    print('The IPv4 address you entered converted to binary is {}.{}.{}.{}'.format(u_bin[:7], u_bin[8:15], u_bin[16:23], u_bin[24:]))
    print('The network address converted to binary is {}.{}.{}.{}'.format(n_bin[:7], n_bin[8:15], n_bin[16:23], n_bin[24:]))
    
    
main()



