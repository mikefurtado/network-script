'''
Script to accept an IPv4 address, determine what network it belongs to,
and display both the IP address and its network back to the user.

#This is the preliminary step to creating a more involved script#
'''
import ipaddress    #import the ipaddress module

#main function definition
def main():
    print('#' * 65)                                                            #display a header
    print('Enter IPv4 address in the following format: X.X.X.X/CIDR')
    print('#' * 65)

    user_address = get_address()    #call get_address function to get IP address from user

    network_address = get_network(user_address) #call get_network function to get network address

    display_results(user_address, network_address)               #call dispaly function to display info to user

    search_file(network_address)

#function definitions
def get_address():
    address = input('Enter an IPv4 address and CIDR value in the above format: ')   #function to prompt user for an IP address
    ip_address = ipaddress.ip_interface(address)                                    #comment on this line
    return ip_address                                                               #return value to main()
 
def get_network(user_address):                                                      #function to determine the network address based on user entered IP address
    net_address = user_address.network                                              #determine the network address using the .network method on the user supplied IP address
    return net_address                                                              #return value to main()

def display_results(user_address, network_address):                                 #function to display information back to user
    print('The IPv4 address you entered was {}'.format(user_address))
    print('The network the address belongs to is {}'.format(network_address))

def search_file(network_address):
    net_add_string = str(network_address)
    searchfile = open('ip-ranges.json', 'r')
    for line in searchfile:
        if net_add_string in line:
            print ('address is valid')

    searchfile.close()
    
    
main()



